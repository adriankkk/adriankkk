browser >
mongo_express [external service] > mongo express [pod] > mongoDB (db url) [internal service] > mongoDB [pod]

# current config
kubectl get all

# 1 - Apply SECRET
kubectl apply -f kube_mongo-secret.yaml
kubectl get secret

# 2 - Create DEPLOMENT from image [image:mongo] [name:mongodb]
kubectl apply -f kube_mongo-pod-deploy.yaml
## reference secret in deployment
env:
        - name: MONGO_INITDB_ROOT_USERNAME
          valueFrom:
           secretKeyRef:
            name: mongodb-secret
            key: mongo-root-username
...
kubect get deployment

# 2 - Create internal SERVICE configuration
kubectl apply -f kube_mongo-pod-deploy.yaml[with-service_code-added]
---
apiVersion: v1
kind: Service
metadata:
  name: mongodb-service
spec:
  selector:
    app: mongodb
  ports:
    - protocol: TCP
      port: 27017 # service port !
      targetPort: 27017  # containerPort from deployment !
...
kubectl describe service mongodb-service
TargetPort:        27017/TCP
Endpoints:         10.1.0.16:27017 [pod_connected]
...

# 3 - Create DEPLOYMENT mongo-express and service
- to which db it should connect to?
- credentials ?
kubectl apply -f mongo-express-deploy-srv.yaml

# 4 - Apply CONFIGMAP
kubectl apply -f mongo-configmap.yaml

# 5 - minikube service mongo-express-service
minikube service mongo-express-service
