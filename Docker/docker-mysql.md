# create docker network
docker network create net-joomla

#run mysql link
docker run -d \
--name mysqldb \
-e MYSQL_ROOT_PASSWORD=password \
-p 3306:3306 \
mysql:5.7

#run joomla link
docker run -d \
--name joomla \
-p 8082:80 \
--network net-joomla \
--link mysqldb:mysql \
joomla

#run mysql net
docker run -d \
--name mysqldb \
-e MYSQL_ROOT_PASSWORD=password \
-p 3306:3306 \
--network net-joomla \
mysql:5.7

IPAddress": "172.19.0.2",

#run joomla net
docker run -d \
--name joomla \
-p 8082:80 \
--network net-joomla \
-e JOOMLA_DB_HOST=mysqldb \
-e JOOMLA_DB_USER=root \
-e JOOMLA_DB_PASSWORD=password \
joomla

"IPAddress": "172.19.0.3"

#run phpmyadmin
docker run -d \
--name phpmyadmin \
--link mysqldb:mysql \
-e PMA_USER=root \
-e PMA_PASSWORD=password \
-p 8083:80 \
--network net-joomla \
phpmyadmin

-e PMA_HOST=mysqldb \
-e PMA_USER=root \
-e PMA_PASSWORD=password \
