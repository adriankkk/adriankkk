docker -v #show version of docker
## NETWORK ##
docker network ls
#ip
docker container inspect mysqldb | grep -i IPAddress
## IMAGES ##
docker images #show images
docker
#Build an image from a Dockerfile (. = dir of Dockerfile!)
docker build -t my-app:1.0 .
## CONTAINERS ##
docker run -i -t ubuntu /bin/bash
docker run -d redis #run new container using image
docker run p6000:6750 -d redis #run new container with port binding
docker stop c4b9a583d0d4 #stop running container
docker start c4b9a583d0d4 #stop suspended container
docker ps #show running container
docker ps -a #show all used container
#remove
docker ps -q | xargs docker rm //remove all container
docker ps -a -q --filter ancestor=joomla:3.9 | xargs docker rm
docker ps -a -q -f status=exited | xargs docker rm
docker rm -f mongo | remove container
##run linux with shell (-it = interactive terminal)
docker run -i -t alpine /bin/sh
docker run -i -t ubuntu /bin/bash
##compose
docker-compose -f joomla-compose.yaml up
docker-compose -f joomla-compose.yaml down
0
